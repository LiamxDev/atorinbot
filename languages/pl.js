let polish = {}

polish.minutes = 'minut'

polish.advert = {}
polish.advert.info = '📢 advert <tekst> - Tworzy ogłoszenie.'
polish.advert.usage = 'Poprawne użycie: &advert <tekst>'
polish.advert.permission = 'Nie posiadasz dostępu do tej komendy. Wymagane uprawnienie:'
polish.advert.title = 'Ogłoszenie'

polish.lang = {}
polish.lang.info = 'lang <język> - Zmienia język bota na serwerze.'
polish.lang.usage = 'Poprawne użycie: &lang <język>'
polish.lang.permission = `Nie posiadasz dostępu do tej komendy. Wymagane uprawnienie:`
polish.lang.success = 'Zmieniono język bota na '
polish.lang.fail = 'Wystąpił błąd przy zmianie języka, spróbuj jeszcze raz.'

polish.avatar = {}
polish.avatar.info = '🤳 avatar @użytkownik - Zwraca awatar oznaczonego użytkownika.'
polish.avatar.usage = 'Poprawne użycie: &avatar @użytkownik'
polish.avatar.title = 'Avatar użytkownika '

polish.bot = {}
polish.bot.info = 'ℹ️ bot - Informacje na temat AtorinBot.'
polish.bot.title = 'Informacje o bocie'
polish.bot.servers = 'Serwery'
polish.bot.author = 'Autor'
polish.bot.page = 'Strona'
polish.bot.open = 'Odwiedź'

polish.help = {}
polish.help.title = 'Lista komend AtorinBot'
polish.help.media = 'Media'
polish.help.tools = 'Narzędzia'
polish.help.info = 'Informacje'
polish.help.other = 'Różne'
polish.help.page2 = 'Wpisz &help 2 aby zobaczyć następną stronę.'
polish.help.page3 = 'Wpisz &help 3 aby zobaczyć następną stronę.'
polish.help.page4 = 'Wpisz &help 4 aby zobaczyć następną stronę.'

polish.poll = {}
polish.poll.info = '❓ poll <pytanie> - Tworzy ankietę Tak/Nie.'
polish.poll.usage = 'Poprawne użycie: &poll <pytanie>'
polish.poll.title = 'Głosowanie stworzone przez użytkownika '
polish.poll.question = 'Pytanie:'

polish.flip = {}
polish.flip.info = '👛 flip - Rzuca monetą.'
polish.flip.title = 'Rzut monetą'
polish.flip.result = 'Wynik:'
polish.flip.heads = 'Orzeł'
polish.flip.tails = 'Reszka'
polish.flip.retry = 'Spróbuj jeszcze raz.'

polish.calc = {}
polish.calc.info = '➗ calc <liczba> <znak> <liczba> - Wykonuje działanie i wysyła wynik.'
polish.calc.usage = 'Poprawne użycie: &calc <liczba> <znak> <liczba>'
polish.calc.title = 'Kalkulator'
polish.calc.data = 'Dane: '
polish.calc.result = 'Wynik: '

polish.cc = {}
polish.cc.info = '💰 cc <ilość> <waluta> <waluta> - Konwertuje walutę na inną.'
polish.cc.usage = 'Poprawne użycie: &cc <ilość> <waluta> <docelowa waluta>'
polish.cc.currecies = 'Wprowadź poprawną walutę. Listę walut znajdziesz tutaj: '
polish.cc.title = 'Konwerter walut'
polish.cc.data = 'Podana ilość i waluta:'
polish.cc.covert = 'Konwersja do:'
polish.cc.result = 'Wynik:'

polish.fortnite = {}
polish.fortnite.info = '🏆 fortnite <gracz> <platforma(pc/psn/xbl)> - Wysyła statystyki gracza w grze Fortnite'
polish.fortnite.usage = 'Poprawne użycie: &fortnite <gracz> <platforma(pc/psn/xbl)>'
polish.fortnite.title = ` statystyki gracza w grze Fortnite`
polish.fortnite.played = 'Zagranych meczy:'
polish.fortnite.wins = 'Wygranych meczy:'
polish.fortnite.kills = 'Ilość zabójstw:'
polish.fortnite.kd = 'Zabójstwa/śmierci:'
polish.fortnite.notfound = 'Nie znaleziono użytkownika.'

polish.pastebin = {}
polish.pastebin.info = '📝 pastebin - Tworzy nowy tekst w serwisie Pastebin.'
polish.pastebin.usage = 'Poprawne użycie: &pastebin <tekst>'
polish.pastebin.success = `, utworzono! Oto link: `
polish.pastebin.fail = 'Nie udało się utworzyć pasty.'

polish.weather = {}
polish.weather.info = '⛅ weather <miasto> - Wysyła informacje o pogodzie.'
polish.weather.usage = 'Poprawne użycie: &weather <miejscowość>'
polish.weather.notfound = 'Nie znaleziono informacji w podanej miejscowości.'
polish.weather.title = 'Pogoda w miejscowości '
polish.weather.main = 'Pogoda: '
polish.weather.temp = 'Temperatura: '
polish.weather.pressure = 'Ciśnienie: '
polish.weather.humidity = 'Wilgotność: '
polish.weather.wind = 'Wiatr: '

polish.groot = {}
polish.groot.info = '🌌 groot <wiadomość> - Mów jak Groot!'
polish.groot.message = 'Jestem Groooot!'

polish.gif = {}
polish.gif.info = '🎦 gif <tag> - Wysyła gif z podanym tagiem.'
polish.gif.usage = 'Poprawne użycie: &gif <tag>'
polish.gif.category = 'Kategoria:'

polish.shiba = {}
polish.shiba.info = '🐕 shiba - Wysyła losowy obrazek z Shiba Inu.'
polish.shiba.title = 'Losowy obrazek z Shiba Inu'

polish.welcome = {}
polish.welcome.info = 'Ustawia kanał do wysyłania przywitań nowych członków serwera'
polish.welcome.success = 'Pomyślnie ustawiono! Teraz każdy nowy użytkownik, będzie przywitany przez bota na tym kanale.'
polish.welcome.fail = 'Wystąpił błąd. Spróbuj jeszcze raz.'
polish.welcome.permission = 'Nie posiadasz dostępu do tej komendy. Wymagane uprawnienie: '

polish.delmsg = {}
polish.delmsg.info = 'Usuwa podaną liczbę wiadomości na kanale.'
polish.delmsg.usage = 'Poprawne użycie: &delmsg <liczba>'
polish.delmsg.permission = 'Nie posiadasz dostępu do tej komendy. Wymagane uprawnienie: '
polish.delmsg.noperm = 'Bot nie posiada uprawnień do wykonania tej czynności. Sprawdź rolę Atorin i przyznaj odpowiednie uprawnienie.'

polish.wtc = {}
polish.wtc.info = 'Wysyła losowy commit'
polish.wtc.title = 'Jaki commit?'

polish.greeting = {}
polish.greeting.title = 'Witaj na '
polish.greeting.description = 'Życzę miłej zabawy.'

polish.server = {}
polish.server.info = 'Zwraca informacje o serwerze'
polish.server.title = 'Informacje o serwerze '
polish.server.owner = 'Właściciel:'
polish.server.created = 'Utworzony:'
polish.server.members = 'Członków:'

polish.user = {}
polish.user.info = 'Zwraca informacje o użytkowniku.'
polish.user.usage = 'Poprawne użycie: &user @użytkownik'
polish.user.title = 'Informacje o użytkowniku '
polish.user.joined = 'Dołączył:'
polish.user.muted = 'Wyciszony:'
polish.user.role = 'Rola:'

polish.logs = {}
polish.logs.info = ''
polish.logs.permission = 'Nie posiadasz dostępu do tej komendy. Wymagane uprawnienie: '
polish.logs.fail = 'Wystąpił błąd. Spróbuj jeszcze raz.'
polish.logs.success = 'Pomyślnie ustawiono. Wszystkie zdarzenia będą teraz wysyłane na ten kanał.'

module.exports = polish
