let english = {}

english.minutes = 'minutes'

english.advert = {}
english.advert.info = '📢 advert <tekst> - Creates advertisement.'
english.advert.usage = 'Correct usage: &advert <text>'
english.advert.permission = `You don't have permission to use this command. Required permission:`
english.advert.title = 'Advertisement'

english.lang = {}
english.advert.info = 'lang <język> - Change bot language on this server.'
english.lang.usage = 'Correct usage: &lang <language>'
english.lang.permission = `You don't have permission to use this command. Required permission:`
english.lang.success = 'Changed bot language to '
english.lang.fail = 'An error occurred while changing the language, try again'

english.avatar = {}
english.avatar.info = '🤳 avatar @user - Sends avatar.'
english.avatar.usage = 'Correct usage: &avatar @user'
english.avatar.title = 'Avatar of '

english.bot = {}
english.bot.info = 'ℹ️ bot - Informations about AtorinBot.'
english.bot.title = 'Info about bot'
english.bot.servers = 'Servers'
english.bot.author = 'Author'
english.bot.page = 'Website'
english.bot.open = 'Open'

english.help = {}
english.help.title = 'Command list of AtorinBot'
english.help.media = 'Media'
english.help.tools = 'Tools'
english.help.info = 'Informations'
english.help.other = 'Other'
english.help.page2 = 'Send &help 2 to see the next page.'
english.help.page3 = 'Send &help 3 to see the next page.'
english.help.page4 = 'Send &help 4 to see the next page.'

english.poll = {}
english.poll.info = '❓ poll <question> - Makes poll Yes/No.'
english.poll.usage = 'Correct usage: &poll <question>'
english.poll.title = 'Voting created by the user '
english.poll.question = 'Question:'

english.flip = {}
english.flip.info = '👛 flip - Flipping a coin.'
english.flip.title = 'Flipping a coin'
english.flip.result = 'Result:'
english.flip.heads = 'Heads'
english.flip.tails = 'Tails'
english.flip.retry = 'Try again.'

english.calc = {}
english.calc.info = '➗ calc <number> <sign> <number> - Calculates the operation and send the result.'
english.calc.usage = 'Correct usage: &calc <number> <sign> <number>'
english.calc.title = 'Calculator'
english.calc.data = 'Data: '
english.calc.result = 'Result: '

english.cc = {}
english.cc.info = '💰 cc <amount> <currency> <currency> - Converts currency to another.'
english.cc.usage = 'Correct usage: &cc <amount> <currency> <currency>'
english.cc.currecies = 'Enter the correct currency. You can find the list of currencies here: '
english.cc.title = 'Currency converter'
english.cc.data = 'The quantity and currency given:'
english.cc.covert = 'Covert to:'
english.cc.result = 'Result:'

english.fortnite = {}
english.fortnite.info = '🏆 fortnite <player> <platrform(pc/psn/xbl)> - Send player statistics in the Fortnite game'
english.fortnite.usage = 'Correct usage: &fortnite <player> <platrform(pc/psn/xbl)>'
english.fortnite.title = ` player statistics in Fortnite game`
english.fortnite.played = 'Matches played:'
english.fortnite.wins = 'Wins:'
english.fortnite.kills = 'Kills:'
english.fortnite.kd = 'Kills/deaths:'
english.fortnite.notfound = 'Player not found.'

english.pastebin = {}
english.pastebin.info = '📝 pastebin - Creates a new text on the Pastebin website.'
english.pastebin.usage = 'Correct usage: &pastebin <text>'
english.pastebin.success = `, created! Link here: `
english.pastebin.fail = 'The paste could not be created.'

english.weather = {}
english.weather.info = '⛅ weather <city> - Sends weather information.'
english.weather.usage = 'Correct usage: &weather <city>'
english.weather.notfound = 'No information found in the given city.'
english.weather.title = 'Weather in '
english.weather.main = 'Weather: '
english.weather.temp = 'Temperature: '
english.weather.pressure = 'Pressure: '
english.weather.humidity = 'Humidity: '
english.weather.wind = 'Wind: '

english.groot = {}
english.groot.info = '🌌 groot <text> - Talk like Groot!'
english.groot.message = 'Iam Groooot!'

english.gif = {}
english.gif.info = '🎦 gif <tag> - Sends a gif with the given tag.'
english.gif.usage = 'Correct usage: &gif <tag>'
english.gif.category = 'Category:'

english.shiba = {}
english.shiba.info = '🐕 shiba - Send a random picture of Shiba Inu.'
english.shiba.title = 'Random picture of Shiba Inu'

english.welcome = {}
english.welcome.info = 'Sets the channel for sending greetings to new server members'
english.welcome.success = 'Successfully set! Now every new user will be greeted by the bot on this channel.'
english.welcome.fail = 'An error occured. Try again.'
english.welcome.permission = 'You do not have access to this command. Required permission: '

english.delmsg = {}
english.delmsg.info = 'Removes the specified number of messages on the channel.'
english.delmsg.usage = 'Correct usage: &delmsg <number>'
english.delmsg.permission = 'You do not have access to this command. Required permission: '
english.delmsg.success = ' has deleted the following number of messages on this channel: '
english.delmsg.noperm = 'Bot does not have permission to do this. Check the role of Atorin and grant the appropriate permission.'

english.wtc = {}
english.wtc.info = 'Sends a random commit'
english.wtc.title = 'What the commit?'

english.greeting = {}
english.greeting.title = 'Welcome on '
english.greeting.description = 'Have a nice day.'

english.server = {}


english.user = {}
english.user.info = 'Zwraca informacje o użytkowniku.'
english.user.usage = 'Correct usage: &user @user'
english.user.title = 'Informations about '
english.user.joined = 'Joined:'
english.user.muted = 'Muted:'
english.user.role = 'Role:'

module.exports = english
