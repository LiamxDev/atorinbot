const { Client } = require('fortnite')
const fortnite = new Client(process.env.fortnite)
const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.fortnite.usage])
      return
    }
    if (extras[2][1] !== 'pc' && extras[2][1] !== 'psn' && extras[2][1] !== 'xbl') {
      resolve([lang.fortnite.usage])
      return
    }
    fortnite.user(extras[2][0], extras[2][1]).then(data => {
      const embed = new Discord.RichEmbed()
      embed.setAuthor(`🏆 ` + `${extras[2][0]}` + lang.fortnite.title, extras[1].user.avatarURL)
      embed.addField('🎲 ' + lang.fortnite.played, data.stats.lifetime[7]['Matches Played'])
      embed.addField('✅ ' + lang.fortnite.wins, data.stats.lifetime[8]['Wins'])
      embed.addField('🔫 ' + lang.fortnite.kills, data.stats.lifetime[10]['Kills'])
      embed.addField('💀 ' + lang.fortnite.kd, data.stats.lifetime[11]['K/d'])
      embed.setFooter('AtorinBot by LiamxDev')
      embed.setThumbnail('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTK36Fe496FTAxASKyX1U6lVanHUFIIpuWtRsJGjWSghN16vijc')
      embed.setColor('c832ff')
      resolve([embed])
    }).catch(error => {
      resolve([lang.fortnite.notfound])
    })
  })
}
