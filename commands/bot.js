const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    const embed = new Discord.RichEmbed()
    embed.setAuthor('ℹ ' + lang.bot.title, extras[1].user.avatarURL)
    embed.addField('Uptime', Math.floor(extras[1].uptime / 1000 / 60) + lang.minutes, true)
    embed.addField('Ping', Math.round(extras[1].ping), true)
    embed.addField(lang.bot.servers, extras[1].guilds.size, true)
    embed.addField(lang.bot.author, 'LiamxDev', true)
    embed.addField(lang.bot.page, `[${lang.bot.open}](https://bot.liamxdev.ovh/)`, true)
    embed.addField('Github', `[${lang.bot.open}](https://github.com/LiamxDev/AtorinBot)`, true)
    embed.setThumbnail(extras[1].user.avatarURL)
    embed.setColor('0a95db')
    resolve([embed])
  })
}
