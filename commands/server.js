const Discord = require('discord.js')
module.exports = exports = function (extras){
    return new Promise(function (resolve, reject){
        let message = extras[0]
        let client = extras[1]
        const embed = new Discord.RichEmbed()
        embed.setTitle('Informacje o serwerze ' + message.guild.name, client.user.avatarURL)
        embed.setThumbnail(message.guild.iconURL)
        embed.addField('Właściciel', message.guild.owner.user.username, true)
        embed.addField('Utworzony', message.guild.createdAt, true)
        embed.addField('Członkowie', message.guild.memberCount, true)
        embed.addField('Region', message.guild.region, true)
        embed.addField('ID', message.guild.id, true)
        embed.setFooter('AtorinBot by LiamxDev')
        resolve([embed])
    })
}