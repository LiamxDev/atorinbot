const request = require('request')
const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    request('http://shibe.online/api/shibes?count=1', function (err, res, body) {
      if (err) console.log(err)
      let data = JSON.parse(body)
      const embed = new Discord.RichEmbed()
      embed.setAuthor('🐕 ' + lang.shiba.title, extras[1].user.avatarURL)
      embed.setImage(data[0])
      embed.setFooter('AtorinBot by LiamxDev')
      embed.setColor('e5ce85')
      resolve([embed])
    })
  })
}
