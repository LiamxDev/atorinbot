const request = require('request')
const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0 || isNaN(extras[2][0]) || typeof extras[2][1] === 'undefined' || typeof extras[2][2] === 'undefined') {
      resolve([lang.cc.usage])
      return
    }
    let from = extras[2][1].toUpperCase()
    let to = extras[2][2].toUpperCase()
    let options = {
      url: 'https://free.currencyconverterapi.com/api/v5/currencies',
      json: true
    }
    request(options, function (err, res, body) {
      if (err) console.log(err)
      if (!(from in body.results) || !(to in body.results)) {
        resolve([lang.cc.currencies + 'https://bot.liamxdev.ovh/currencies'])
        return false
      } else {
        request('https://free.currencyconverterapi.com/api/v5/convert?q=' + from + '_' + to + '&compact=ultra', function (err, res, body) {
          if (err) console.log(err)
          let data = JSON.parse(body)
          let total = extras[2][0] * data[from + '_' + to]
          const embed = new Discord.RichEmbed()
          embed.setAuthor('💰 ' + lang.cc.title, extras[1].user.avatarURL)
          embed.addField(lang.cc.data, extras[2][0] + from)
          embed.addField(lang.cc.convert, to)
          embed.addField(lang.cc.result, Math.round(total * 100) / 100 + to)
          embed.setFooter('AtorinBot by LiamxDev')
          embed.setColor('e8cf14')
          resolve([embed])
        })
      }
    })
  })
}
