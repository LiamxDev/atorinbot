const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let args = extras[2]
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    const embed = new Discord.RichEmbed()
    embed.setAuthor(lang.help.title, extras[1].user.avatarURL)
    switch (true) {
      case args[0] === '2':
        embed.setDescription(`2/4 ${lang.help.tools}: \n \n ${lang.poll.info} \n ${lang.flip.info} \n ${lang.calc.info} \n ${lang.cc.info} \n ${lang.fortnite.info} \n ${lang.pastebin.info} \n ${lang.advert.info} \n \n ${lang.help.page3}`)
        break
      case args[0] === '3':
        embed.setDescription(`3/4 ${lang.help.info}: \n \n ${lang.bot.info} \n ${lang.weather.info} \n \n ${lang.help.page4}`)
        break
      case args[0] === '4':
        embed.setDescription(`4/4 ${lang.help.other}: \n \n ${lang.groot.info}`)
        break
      default:
        embed.setDescription(`1/4 ${lang.help.media}: \n \n ${lang.gif.info} \n ${lang.shiba.info} \n ${lang.avatar.info} \n \n ${lang.help.page2}`)
    }
    embed.setFooter('AtorinBot by LiamxDev')
    embed.setColor('680000')
    resolve([embed])
  })
}
