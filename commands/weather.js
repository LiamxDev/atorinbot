const request = require('request')
const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.weather.usage])
      return
    }
    request('http://api.openweathermap.org/data/2.5/weather?q=' + encodeURIComponent(extras[0].content.toLowerCase().replace('&weather ', '')) + '&appid=' + process.env.weather + '&units=metric', function (err, res, body) {
      if (err) console.log(err)
      var data = JSON.parse(body)
      if (data.cod === '404') {
        resolve([lang.weather.notfound])
      } else {
        const embed = new Discord.RichEmbed()
        embed.setAuthor('⛅ ' + lang.weather.title + extras[0].content.replace('&weather ', '') + ' 🌇', extras[1].user.avatarURL)
        embed.addField('☁️ ' + lang.weather.main, data.weather[0].main)
        embed.addField('🌡 ' + lang.weather.temp, data.main.temp + '°C')
        embed.addField('🎈 ' + lang.weather.pressure, data.main.pressure + 'hPa')
        embed.addField('💧 ' + lang.weather.humidity, data.main.humidity + '%')
        embed.addField('💨 ' + lang.weather.wind, data.wind.speed + 'm/s')
        embed.setFooter('AtorinBot by LiamxDev')
        embed.setColor('00c4a6')
        resolve([embed])
      }
    })
  })
}
