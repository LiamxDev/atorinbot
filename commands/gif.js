const request = require('request')
const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.gif.usage])
      return
    }
    request('http://api.giphy.com/v1/gifs/random?api_key=' + process.env.giphy + '&tag=' + encodeURIComponent(extras[0].content.replace('&gif', '')), function (err, res, body) {
      if (err) console.log(err)
      const embed = new Discord.RichEmbed()
      let result = JSON.parse(body)
      embed.setAuthor('🎦 Gif', extras[1].user.avatarURL)
      embed.addField(lang.gif.category, extras[0].content.replace('&gif', ''))
      embed.setImage(result.data.image_url)
      embed.setFooter('AtorinBot by LiamxDev')
      embed.setColor('919191')
      resolve([embed])
    })
  })
}
