const Discord = require('discord.js')
const client = new Discord.Client()
const fs = require('fs')
const path = require('path')
const db = require('./database.js')
const pool = db.getPool()

let commands = {}
let dirname = path.join(__dirname, 'commands')

fs.readdirSync(dirname).forEach(file => {
  let filename = file.replace('.js', '')
  commands[filename] = require(path.join(dirname, file))
})

client.on('message', message => {
  const args = message.content.trim().split(/ +/g)
  const command = args.shift().replace('&', '').toLowerCase()
  if (message.content.startsWith('&')) {
    if (command in commands) {
      pool.query(`SELECT language FROM servers WHERE server = ${message.guild.id}`, function (err, result) {
        if (err) throw err
        let extras = [
          message,
          client,
          args,
          result[0].language
        ]
        Promise.resolve(commands[command](extras)).then(function (res, rej) {
          message.channel.send(res[0]).then(function (message) {
            if (typeof res[1] !== 'undefined') {
              for (var i = 0; i < res[1].length; i++) {
                message.react(res[1][i])
              }
            }
          })
        })
      })
    }
  }
})

client.on('ready', () => {
  console.log(`Zalogowano jako ${client.user.tag}!`)
  client.user.setPresence({game: {name: 'v2.2 | &help'}, status: 'online'})
})

client.on('guildCreate', guild => {
  console.log(`Dodano bota na serwer ${guild.name}!`)
  pool.query(`INSERT INTO servers (server, language, name) VALUES ('${guild.id}', 'pl', '${guild.name}')`, function (err, result) {
    if (err) throw err
    console.log('Dodano wpis do bazy danych')
  })
})

client.on('guildDelete', guild => {
  console.log(`Usunięto bota z serwera ${guild.name}.`)
  pool.query(`DELETE FROM servers WHERE server = ${guild.id}`, function (err, result) {
    if (err) throw err
    console.log('Usunięto wpis z bazy danych')
  })
})

client.on('guildMemberAdd', member => {
  pool.query(`SELECT logs FROM servers WHERE server = ${member.guild.id}`, function (err, result) {
    if (err) throw err
    if (typeof member.guild.channels.get(result[0].logs) === 'undefined') return
    const embed = new Discord.RichEmbed()
    embed.setAuthor('Witaj na ' + member.guild.name, client.user.avatarURL)
    embed.setDescription(`<@${member.id}>\n${new Date(Date.now()).toLocaleString()}`)
    embed.setThumbnail(member.user.avatarURL)
    embed.setColor('79e200')
    embed.setFooter('AtorinBot by LiamxDev')
    member.guild.channels.get(result[0].logs).send(embed)
  })
})

client.on('guildMemberRemove', member => {
  pool.query(`SELECT logs FROM servers WHERE server = ${member.guild.id}`, function (err, result) {
    if (err) throw err
    if (typeof member.guild.channels.get(result[0].logs) === 'undefined') return
    const embed = new Discord.RichEmbed()
    embed.setAuthor('Użytkownik opuścił ' + member.guild.name, client.user.avatarURL)
    embed.setDescription(`<@${member.id}>\n${new Date(Date.now()).toLocaleString()}`)
    embed.setThumbnail(member.user.avatarURL)
    embed.setColor('RED')
    embed.setFooter('AtorinBot by LiamxDev')
    member.guild.channels.get(result[0].logs).send(embed)
  })
})

client.on('messageDelete', message => {
  pool.query(`SELECT logs FROM servers WHERE server = ${message.guild.id}`, function (err, result) {
    if (err) throw err
    if (typeof message.guild.channels.get(result[0].logs) === 'undefined') return
    const embed = new Discord.RichEmbed()
    let channelId = message.guild.channels.find('name', message.channel.name)
    embed.setAuthor('Usunięta wiadomość', client.user.avatarURL)
    embed.setDescription(`Treść: ${message.content}\nAutor: <@${message.author.id}>\nKanał: ${channelId}\n${new Date(Date.now()).toLocaleString()}`)
    embed.setThumbnail(message.author.avatarURL)
    embed.setColor('RED')
    embed.setFooter('AtorinBot by LiamxDev')
    message.guild.channels.get(result[0].logs).send(embed)
  }) 
})

client.on('guildBanAdd', (guild, user) => {
  pool.query(`SELECT logs FROM servers WHERE server = ${guild.id}`, function (err, result) {
    if (err) throw err
    if (typeof guild.channels.get(result[0].logs) === 'undefined') return
    const embed = new Discord.RichEmbed()
    embed.setAuthor('Zbanowany użytkownik', client.user.avatarURL)
    embed.setDescription(`<@${user.id}>\n${new Date(Date.now()).toLocaleString()}`)
    embed.setThumbnail(user.avatarURL)
    embed.setColor('RED')
    embed.setFooter('AtorinBot by LiamxDev')
    guild.channels.get(result[0].logs).send(embed)
  }) 
})

client.on('guildBanRemove', (guild, user) => {
  pool.query(`SELECT logs FROM servers WHERE server = ${guild.id}`, function (err, result) {
    if (err) throw err
    if (typeof guild.channels.get(result[0].logs) === 'undefined') return 
    const embed = new Discord.RichEmbed()
    embed.setAuthor('Odbanowany użytkownik', client.user.avatarURL)
    embed.setDescription(`<@${user.id}>\n${new Date(Date.now()).toLocaleString()}`)
    embed.setThumbnail(user.avatarURL)
    embed.setColor('RED')
    embed.setFooter('AtorinBot by LiamxDev')
    guild.channels.get(result[0].logs).send(embed)
  }) 
})

client.login(process.env.token)
